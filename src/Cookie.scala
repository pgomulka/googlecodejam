/**
 * Created by przemek on 14.03.15.
 */
object Cookie {


  var prev: Int = 9999999;

  def main(args: Array[String]) {
    val C = 500
    val F = 4
    val X = 2000

    println(Tmax(0, C, F, X))
  }

  def Tmax(N: Int, C: Int, F: Int, X: Double): Double = {

    def T(N: Int, X: Double): Double = {
      if (N == 0)
        return X / 2

      return T(N - 1, C) + X / (F * N + 2)
    }

    if (T(N, X) > T(N + 1, X))
      return Tmax(N + 1, C, F, X)
    return T(N, X)
  }


}
