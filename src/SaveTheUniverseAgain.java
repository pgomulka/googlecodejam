import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class SaveTheUniverseAgain {

    public static void main(String[] args) {
//        System.out.println(solve(1,"CS"));
//        System.out.println(solve(2,"CS"));
//        System.out.println(solve(1,"SS"));
//        System.out.println(solve(6,"SCCSSC"));
//        System.out.println(solve(2,"CC"));
//        System.out.println(solve(3,"CSCSS"));
//
        Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
        int t = in.nextInt(); // Scanner has functions to read ints, longs, strings, chars, etc.
        for (int i = 1; i <= t; ++i) {

            int n = in.nextInt();
            String m = in.nextLine();
            String result = solve(n, m);
            System.out.println("Case #" + i + ": " +result);
        }
    }

    private static String solve(int d, String line) {
        char[] s = line.toCharArray();
        if (calc(s) <= d) {
            return "0";
        }
        int c = 0;
        for(int j=0;j<s.length;j++) {
            for (int i = j; i < s.length; i++) {
                if (s[i] == 'S' && i - 1 >= 0 && s[i - 1] == 'C') {
                    swap(s, i, i - 1);
                    c++;
                    if (calc(s) <= d) {
                        return ""+c;
                    }
                }
            }
        }
        return "IMPOSSIBLE";
    }

    private static void swap(char[] s, int a, int b) {
        char t = s[a];
        s[a]=s[b];
        s[b]=t;
    }

    private static int calc(char[] s) {
        int cp = 1;
        int sum = 0;
        for (int i = 0; i < s.length; i++) {
            if (s[i] == 'S')
                sum += cp;
            else if (s[i] == 'C')
                cp *= 2;
        }
        return sum;
    }
}
