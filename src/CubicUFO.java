import java.util.Arrays;

public class CubicUFO {
    public static void main(String[] args) {
//        solve(1.414213);
//        solve(1);
        System.out.println((long)Math.ceil(15/2.0));
//        float a = (float)
//                (
//                        (1.0 * num-(L * (L + 1)) / 2) / (L + 1)
//                );
//        if (a-(int)a == 0.0)
//            count++;
    }

    private static void solve(double expectedLength) {
        double[] t1 = {0.5, 0.5};
        double[] t2 = {-0.5, 0.5};
        double[] t3 = {-0.5, -0.5};
        double[] t4 = {0.5, -0.5};
        double b = 0, e = Math.PI / 4.0;
        double d = 0;
        // for (double d = 0; d < Math.PI / 4; d += 0.05) {
        while (true) {

            double[] nt1 = rotate(t1, d);
            double[] nt2 = rotate(t2, d);
            double[] nt3 = rotate(t3, d);
            double[] nt4 = rotate(t4, d);

            double l = nt4[0] - nt2[0];
//            System.out.println(d + " " + l);
            if (Math.abs(l - expectedLength) < 0.000_000_001) {
                System.out.println(d + " " + l);
                double[][] array = calcCenters(nt1, nt2, nt4);
                System.out.println(Arrays.toString(array[0]));
                System.out.println(Arrays.toString(array[1]));
                System.out.println(Arrays.toString(array[2]));
                return;
            }
//

            d = d+0.000_000_001;
        }
    }

    private static double[][] calcCenters(double[] nt1, double[] nt2, double[] nt4) {
        return new double[][]{
            {0,0,0.5},//front nothing changes
            {(nt2[0]-nt1[0])/2,(nt2[1]-nt1[1])/2,0.5},//top
            {(nt4[0]-nt1[0])/2,(nt1[1]-nt4[1])/2,0.5}//side
        };
    }

    private static double[] rotate(double[] t1, double d) {
        return new double[]{t1[0] * Math.cos(d) - t1[1] * Math.sin(d),
                t1[0] * Math.cos(d) - t1[1] * Math.sin(d)};
    }
}
