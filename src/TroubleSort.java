import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class TroubleSort {
    public static void main(String[] args) {
        /*
        2
5
5 6 8 4 3
3
8 9 7
         */
//        System.out.println(solve(1, new int[]{1, 2, 3, 4}));
//        System.out.println(solve(1, new int[]{8,9,7}));
//        System.out.println(solve(1, new int[]{5, 6, 8, 4, 3}));
//        System.out.println(solve(1, new int[]{9,8,1,0}));
        Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in),10000));
        int t = in.nextInt(); // Scanner has functions to read ints, longs, strings, chars, etc.
        for (int i = 1; i <= t; ++i) {

            int length = in.nextInt();
            in.nextLine();
            String m = in.nextLine();
            String[] split = m.split(" ");
            int[] arr = new int[length];
            for (int j = 0; j < length; j++) {
                arr[j] = Integer.parseInt(split[j]);
            }
            String result = solve(length, arr);
            System.out.println("Case #" + i + ": " + result);
        }
    }

    private static String solve(int length, int[] L) {
        int[] L1 = new int[(int) Math.ceil(L.length / 2.0)];
        int[] L2 = new int[L.length / 2];
        int c1 = 0, c2 = 0;
        for (int i = 0; i < L.length; i++) {
            if (i % 2 == 0) {
                L1[c1++] = L[i];
            } else {
                L2[c2++] = L[i];
            }
        }
//        System.out.println(Arrays.toString(L1));
//        System.out.println(Arrays.toString(L2));
        Arrays.sort(L1);
        Arrays.sort(L2);

        c1=0;
        c2=0;
        for (int i = 0; i < L.length; i++) {
            if (i % 2 == 0) {
                L[i]=L1[c1++];
            } else {
                L[i]=L2[c2++];
            }
        }

        for (int i = 0; i < L.length-1; i++) {
            if(L[i]>L[i+1]){
                return ""+i;
            }
        }

        return "OK";
    }
}
