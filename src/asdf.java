class asdf {    /*
 * Complete the textQueries function below.

    static Map<String, Integer> splitLine(String line) {
        String[] splitted = line.split(" ");
        Map<String, Integer> map = new HashMap<>();

        for (String s : splitted) {
            // set.add(s);
            map.putIfAbsent(s, 0);
            int current = map.get(s);
            map.put(s, current + 1);
        }
        return map;
    }

    static void textQueries(String[] sentences, String[] queries) {

        List<Map<String, Integer>> splittedSentences = new ArrayList<>();
        for (String sentence : sentences) {
            Map<String, Integer> sentenceWords = splitLine(sentence);
            splittedSentences.add(sentenceWords);
        }

        for (int i = 0; i < queries.length; i++) {
            String query = queries[i];
            boolean found = false;
            Map<String, Integer> splitedQuery = splitLine(query);
            for (int j = 0; j < splittedSentences.size(); j++) {
                //String sentence = sentences[j];
                Map<String, Integer> sentenceWords = splittedSentences.get(j);
                if (sentenceWords.keySet().containsAll(splitedQuery.keySet())) {
                    int min = Integer.MAX_VALUE;
                    found = true;
                    for (String k : splitedQuery.keySet()) {
                        int c = sentenceWords.get(k);
                        min = Math.min(min, c);
                    }
                    for (int r = 0; r < min; r++) {

                        System.out.print(j + " ");
                    }

                }
            }
            if (!found)
                System.out.print("-1");
            System.out.println();


        }


    }

     */
}
