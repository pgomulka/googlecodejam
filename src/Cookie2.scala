/**
 * Created by przemek on 14.03.15.
 */
object Cookie2 {


  var prev: Int = 9999999;

  def main(args: Array[String]) {
    val C = 500
    val F = 4
    val X = 2000

    println(Tmax(0, C, F, X, 9999))
  }

  def Tmax(N: Int, C: Int, F: Int, X: Double, Prev: Double): Double = {

    def Tn_1(X: Double): Double = {
      return Prev
    }

    def Tn( X: Double): Double = {
      if (N == 0)
        return X / 2

      return Tn_1(C) + X / (F * N +   2)
    }

    if (Tn_1(X) > Tn( X))
      return Tmax(N + 1, C, F, X, Tn(X))
    return Tn( X)
  }




}
