public class ConcecutiveSUm {
    /*
     * Complete the consecutive function below.
     */
    static int consecutive(long num) {
        /*
         * Write your code here.
         */
        //int[] prefixSum = new int[num+1];
       /* Map<Long,Long> prefixSum = new HashMap<>();
        prefixSum.put(0L,0L);
        Map<Long,Long> psMap = new HashMap<>();
        for(long i=1;i<=(long)Math.ceil(num/2.0);i++){
            //prefixSum[i]=prefixSum[i-1]+i;
            //psMap.put(prefixSum[i],i);
            prefixSum.put(i,prefixSum.get(i-1)+i);
            psMap.put(prefixSum.get(i-1),i);
        }
        int c = 0;
        System.out.println(prefixSum);
        for(long i=0;i<=(long)Math.ceil(num/2.0);i++){
            //int needed = prefixSum[i]-num;
            long needed = prefixSum.get(i)-num;
            System.out.println(" i"+i+" needed "+needed);
            if(psMap.containsKey(needed)){
                c++;

                System.out.println("found e"+i+" b"+psMap.get(needed));
            }
        }
        return c;*/
        int c = 0;
        for(int L=1;L*(L+1)<2*num;L++){

            float a = (float)
                    (
                            (1.0 * num-(L * (L + 1)) / 2) / (L + 1)
                    );
            if(a - (int)a==0.0)
                c++;

        }
        return c;
    }



}
