import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeSet;

public class SumoLogic1 {
    public static void main(String[] args) {
        System.out.println("he");
//        Integer[] x = digits(0);
//        System.out.println(Arrays.toString(x));
        TreeSet<Integer> x = new TreeSet<>();
        x.add(1);
        x.add(2);
        x.add(3);
        System.out.println(x);
    }

    private static int number(int[] ints) {
        int n =0;
        int p =0;
        for(int i=ints.length-1;i>=0;i--){
            n+=Math.pow(10,p)*ints[i];
            p++;
        }
        return n;
    }

    private static Integer[] digits(int i) {
        ArrayList<Integer> r = new ArrayList<>();

        while(i>=0){
            r.add(0,i%10);
            i = i/10;
        }
        return r.toArray(new Integer[]{});
    }
}
